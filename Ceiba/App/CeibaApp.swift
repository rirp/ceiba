//
//  CeibaApp.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 7/12/20.
//

import SwiftUI

@main
struct CeibaApp: App {
    let persistenceController = PersistenceController.shared
    @ObservedObject var appState: AppState = .shared
    
    init () {
        /// Register `User` services on `ServiceLocator`
        ServiceLocator.register(UserRepository() as IUserRepository)
        ServiceLocator.register(ContentViewModel() as IContentViewModel)
        
        /// Register `Post` services on `ServiceLocator`
        ServiceLocator.register(PostRepository() as IPostRepository)
        ServiceLocator.register(PostViewModel() as IPostViewModel)
    }
    
    var body: some Scene {
        WindowGroup {
            
            ContentView()
                .environment(\.managedObjectContext,
                             persistenceController.container.viewContext)
            
        }
    }
}
