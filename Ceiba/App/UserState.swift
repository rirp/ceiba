//
//  State.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 9/12/20.
//

import SwiftUI

class AppState: ObservableObject {
    static let shared = AppState()
    
    @Published var users = [User]()
    @Published var isLoading = false
    @Published var errorMessage = ""
}
