//
//  State.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 9/12/20.
//

import Foundation

class UserState: ObservableObject {
    @Published var users = [User]()
}
