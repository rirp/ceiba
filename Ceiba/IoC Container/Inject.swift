//
//  Dependency.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 9/12/20.
//

import Foundation

@propertyWrapper
class Inject<T> {
    var wrappedValue: T?
    
    init() {
        if ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil {
            return
        }
        self.wrappedValue = ServiceLocator.resolve()
    }
}
