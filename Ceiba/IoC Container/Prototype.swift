//
//  Prototype.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 9/12/20.
//

import Foundation



protocol Initializable {
    init()
}

class Prototype {
    static func createInstance<T>(typeThing:T.Type) -> T where T:Initializable {
        return typeThing.init()
    }
}
