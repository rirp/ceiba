//
//  DependencyContainer.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 9/12/20.
//

import Foundation

private struct ServiceLocatorStatic {
    static var singleton: ServiceLocator? = nil
}

/// `ServiceLocation` resolver the dependencies that have registered on itself.
public class ServiceLocator {
    private (set) var dependencies = [String: AnyObject]()
    
    static var shared = ServiceLocator()
    
    /// Reset the static `sharedInstance`, for example for testing
    public class func resetSharedInstance() {
        ServiceLocatorStatic.singleton = nil
    }
    
    /// Set `ServiceLocator` instance manually
    public class func setSharedInstance(_ instance: ServiceLocator) {
        ServiceLocatorStatic.singleton = instance
    }
    
    public static func register<T>(_ dependency: T) {
        shared.register(dependency)
    }
    
    public static func resolve<T>() -> T? {
        return shared.resolve()
    }
    
    public static func unregister<T>(_ dependency: T?) {
        shared.unregister(dependency) 
    }
}

extension ServiceLocator {
    private func register<T>(_ dependency: T?) {
        let key = "\(type(of: T.self))"
        dependencies[key] = dependency as AnyObject
    }
    
    private func unregister<T>(_ dependency: T?) {
        let key = "\(type(of: T.self))"
        dependencies.removeValue(forKey: key)
    }
    
    private func resolve<T>() -> T? {
        let key = "\(type(of: T.self))"
        let dependency = dependencies[key]
        
        precondition(dependency != nil, "No Dependency found for \(key), Application must register a dependency before resolving it.")
        
        return dependency as? T
    }
}
