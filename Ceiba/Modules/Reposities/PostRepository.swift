//
//  PostRepository.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 10/12/20.
//

import Foundation
import Alamofire

protocol IPostRepository {
    func getPost(by userId: Int, completion: @escaping ([Post])-> Void)
    func saveOnDevice()
}

class PostRepository: IPostRepository {
    let persistenceController = PersistenceController.shared
    
    func getPost(by userId: Int, completion: @escaping ([Post])-> Void) {
        AF.request("https://jsonplaceholder.typicode.com/posts?userId=\(userId)")
            .validate()
            .responseDecodable(of: [Post].self) { response in
                if let posts = response.value {
                    completion(posts)
                } else {
                    completion([Post]())
                }
            }
    }
    
    func saveOnDevice() {
        persistenceController.saveContext()
    }
    
    deinit {
        ServiceLocator.unregister(self as IPostRepository)
    }
}
