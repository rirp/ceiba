//
//  NetworkRepository.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 9/12/20.
//

import Foundation
import Alamofire
import SwiftUI

protocol IUserRepository: class {
    func getUserFromService(completion: @escaping ([User])-> Void)
    func saveOnDevice()
}

class UserRepository: IUserRepository {
    let persistenceController = PersistenceController.shared
    
    func getUserFromService(completion: @escaping ([User])-> Void) {
        AF.request("https://jsonplaceholder.typicode.com/users")
            .validate()
            .responseDecodable(of: [User].self) { response in
                if let users = response.value {
                    completion(users)
                } else {
                    completion([User]())
                }
            }
    }
    
    func saveOnDevice() {
        persistenceController.saveContext()
    }
}
