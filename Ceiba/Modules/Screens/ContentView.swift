//
//  ContentView.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 7/12/20.
//

import SwiftUI
import Combine
import CoreData

struct ContentView: View {
    
    @FetchRequest(
        entity: User.entity(),
        sortDescriptors: [NSSortDescriptor(key: "name", ascending: true)]
    ) var usersFromCD: FetchedResults<User>
    
    let persistenceController = PersistenceController.shared
    
    /// States
    @State private var isShowing = false
    @ObservedObject var appState: AppState = .shared
    /// Context
    @Environment(\.managedObjectContext) private var viewContext
    /// Injected
    @Inject<IContentViewModel> private var viewModel: IContentViewModel?
    
    
    /// `refresh` action: Scroll down action when `users` are empty
    private func refresh() {
        if usersFromCD.isEmpty {
            self.viewModel?.getUsers()
        } else {
            appState.users = self.viewModel?.convertUser(from: usersFromCD) ?? [User]()
        }
    }
    
    var body: some View {
        VStack {
            if appState.errorMessage.isEmpty {
                UserListView(self.viewModel)
            } else {
                VStack {
                    Spacer()
                    
                    Button(action: { refresh() }) {
                        Image(systemName: "arrow.clockwise.icloud")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 20,height: 20)
                        
                        Text(appState.errorMessage)
                            .font(.headline)
                    }
                    
                    Spacer()
                }
            }
        }
        .onAppear {
            refresh()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
