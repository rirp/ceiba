//
//  PostView.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 10/12/20.
//

import SwiftUI

struct PostView: View {
    @Inject<IPostViewModel> var viewmodel: IPostViewModel?
    @ObservedObject var appState: AppState = .shared
    
    var index: Int
    
    var body: some View {
        LoadingView(isShowing: $appState.isLoading) {
            VStack {
                HStack(alignment: .top) {
                    VStack(alignment: .leading) {
                        HStack {
                            Text("Name: ")
                                .font(.body)
                                .bold()
                            Text(appState.users[index].name ?? "")
                                .font(.body)
                        }
                        HStack {
                            Text("Email: ")
                                .font(.body)
                                .bold()
                            Text(appState.users[index].email ?? "")
                                .font(.body)
                        }
                        HStack {
                            Text("Phone: ")
                                .font(.body)
                                .bold()
                            Text(appState.users[index].phone ?? "")
                                .font(.body)
                        }
                    }
                    
                    
                    Spacer()
                }.padding(.all, 10)
                List(appState.users[index].post?.allObjects as? [Post] ?? [Post]()) { post in
                    VStack(alignment: .leading) {
                        Text(post.title ?? "")
                            .frame(alignment: .center)
                            .multilineTextAlignment(.center)
                            .font(.title)
                        
                        
                        Text(post.body ?? "")
                            .multilineTextAlignment(.center)
                            .font(.body)
                            .padding(.vertical, 3)
                        Spacer()
                    }.padding(.all, 10)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .background(RoundedRectangle(cornerRadius: 15).fill(Color.white).shadow(radius: 8))
                }
                .navigationBarTitle("Posts")
                .onAppear {
                    viewmodel?.getPost(by: Int(appState.users[index].id),
                                       index: index)
                }
            }
        }
    }
}



struct PostView_Previews: PreviewProvider {
    static var previews: some View {
        PostView(index: 0)
    }
}
