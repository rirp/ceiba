//
//  UserItemListView.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 10/12/20.
//

import SwiftUI

struct UserItemListView: View {
    let index: Int
    @ObservedObject var appState: AppState = .shared
    
    var body: some View {
        HStack {
            HStack  {
                VStack(alignment: .leading) {
                    
                    Text(appState.users[index].name ?? "")
                        .font(.body)
                    Text("email: \(appState.users[index].email ?? "")")
                        .font(.caption2)
                    Text("phone: \(appState.users[index].phone ?? "")")
                        .font(.caption)
                }.padding(.leading, 10)
                Spacer()
            }
            VStack(alignment: .trailing) {
                NavigationLink(destination: PostView(index: index)) {
                    Spacer()
                    Text("See posts")
                        .multilineTextAlignment(.center)
                        .font(.caption)
                        .padding(.horizontal, 16)
                        .padding(.vertical, 8)
                        .overlay(
                            RoundedRectangle(cornerRadius: 20)
                                .fill(Color.gray.opacity(0.3))
                        )
                        .foregroundColor(.purple)
                        .frame(width: 100)
                }.frame(maxWidth: .infinity)
            }
        }
        .frame(minWidth: 0,
               maxWidth: .infinity,
               minHeight: 0,
               alignment: .top)
    }
}

struct UserItemListView_Previews: PreviewProvider {
    static var previews: some View {
        UserItemListView(index: 0)
    }
}
