//
//  UserListView.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 10/12/20.
//

import SwiftUI

struct UserListView: View {
    @State private var searchText = ""
    @ObservedObject var appState: AppState = .shared
    var viewModel: IContentViewModel?
    
    init(_ viewModel: IContentViewModel?) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        NavigationView {
            LoadingView(isShowing: $appState.isLoading){
                VStack {
                    SearchBar(text: $searchText)
                    List(appState.users.filter { user in
                        self.viewModel?.filter(with: self.searchText, user: user) ?? true
                    }.enumerated().map({$0}), id: \.element.name) { index, user in
                        UserItemListView(index: index)
                    }.background(Color.red)
                    
                    
                }
                .navigationBarTitle("")
                .navigationBarHidden(true)
                
            }
        }
    }
}


struct UserListView_Previews: PreviewProvider {
    static var previews: some View {
        UserListView(ContentViewModel())
    }
}


