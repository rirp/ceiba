//
//  ContainerViewModel.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 9/12/20.
//

import SwiftUI
import Combine

protocol IContentViewModel: class {
    func getUsers()
    func saveOnDevice()
    func convertUser(from results: FetchedResults<User>) -> [User]
    func filter(with string: String, user: User) -> Bool
}

class ContentViewModel: IContentViewModel {
    @ObservedObject var appState: AppState = .shared
    @Inject() var repository: IUserRepository?
    
    init() {
        
    }
    
    init(repository: IUserRepository) {
        self.repository = repository
    }
    
    func getUsers() {
        appState.isLoading = true
        repository?.getUserFromService { users in
            self.appState.isLoading = false
            if users.isEmpty {
                self.appState.errorMessage = "List is empty"
            } else {
                self.appState.errorMessage = ""
                self.appState.users = users
                self.saveOnDevice()
            }
        }
    }
    
    func saveOnDevice() {
        self.repository?.saveOnDevice()
    }
    
    func convertUser(from results: FetchedResults<User>) -> [User] {
        var users = [User]()
        for user in results {
            users.append(user)
        }
        return users
    }
    
    func filter(with string: String, user: User) -> Bool {
        return string.isEmpty ? true : user.name?.lowercased().contains(string.lowercased()) ?? false
    }
}
