//
//  PostViewModel.swift
//  Ceiba
//
//  Created by Ronald Ivan Ruiz Poveda on 10/12/20.
//

import SwiftUI

protocol IPostViewModel {
    func getPost(by userId: Int, index: Int)
}

class PostViewModel: IPostViewModel {
    @ObservedObject var appState: AppState = .shared
    @Inject<IPostRepository> var repository: IPostRepository?
    
    func getPost(by userId: Int, index: Int) {
        appState.isLoading = true
        repository?.getPost(by: userId) { posts in
            self.appState.isLoading = false
            let users = self.appState.users
            users[index].post = NSSet(array: posts)
            self.appState.users = users
        }
    }
    
    deinit {
        ServiceLocator.unregister(repository)
    }
}
