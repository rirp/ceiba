//
//  Post+CoreDataClass.swift
//  
//
//  Created by Ronald Ivan Ruiz Poveda on 9/12/20.
//
//

import Foundation
import CoreData

@objc(Post)
public class Post: NSManagedObject, Identifiable, Decodable {
    enum CodingKeys: CodingKey {
        case id, title, body
    }
    
    required convenience public init(from decoder: Decoder) throws {
        let context = PersistenceController.shared.container.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: Self.description(), in: context) else { fatalError() }

        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int16.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
        self.body = try container.decode(String.self, forKey: .body)
    }

}
