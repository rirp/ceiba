//
//  User+CoreDataClass.swift
//  
//
//  Created by Ronald Ivan Ruiz Poveda on 9/12/20.
//
//

import Foundation
import CoreData

@objc(User)
public class User: NSManagedObject, Identifiable, Decodable {
    enum CodingKeys: CodingKey {
        case id, name, email, phone
    }
    
    convenience init(id: Int16 = 0, name: String? = nil, email: String? = nil, phone: String? = nil) {
        let context = PersistenceController.shared.container.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: Self.description(), in: context) else { fatalError() }

        self.init(entity: entity, insertInto: context)
        
        self.id = id
        self.name = name
        self.email = email
        self.phone = phone
    }
    
    required convenience public init(from decoder: Decoder) throws {
        let context = PersistenceController.shared.container.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: Self.description(), in: context) else { fatalError() }

        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int16.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.email = try container.decode(String.self, forKey: .email)
        self.phone = try container.decode(String.self, forKey: .phone)
    }
}
