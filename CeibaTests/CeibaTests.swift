//
//  CeibaTests.swift
//  CeibaTests
//
//  Created by Ronald Ivan Ruiz Poveda on 7/12/20.
//

import XCTest
@testable import Ceiba

class CeibaTests: XCTestCase {
    override class func setUp() {
        super.setUp()
    }

    func test_contentViewModel_success() {
        let state = AppState.shared
        
        let repository: IUserRepository = MockUserRepository() as IUserRepository
        let viewmodel: IContentViewModel = ContentViewModel(repository: repository)
        
        viewmodel.getUsers()
        
        XCTAssert(state.users.isEmpty)
    }
    
    func test_getUser_was_successful_with_data() {
        let state = AppState.shared
        
        let repository: IUserRepository = MockUserRepository(type: .full) as IUserRepository
        let viewmodel: IContentViewModel = ContentViewModel(repository: repository)
        
        viewmodel.getUsers()
        
        XCTAssert(!state.users.isEmpty)
    }
    
    func test_filter_method_when_not_match() {
        let state = AppState.shared
        
        let repository: IUserRepository = MockUserRepository(type: .full) as IUserRepository
        let viewmodel: IContentViewModel = ContentViewModel(repository: repository)
        
        viewmodel.getUsers()
        
        let userEmpty = state.users.filter { user in
            viewmodel.filter(with: "?", user: user)
        }
        
        XCTAssert(userEmpty.isEmpty)
    }
    
    func test_filter_method_when_match_4() {
        let state = AppState.shared
        
        let repository: IUserRepository = MockUserRepository(type: .full) as IUserRepository
        let viewmodel: IContentViewModel = ContentViewModel(repository: repository)
        
        viewmodel.getUsers()
        
        let userEmpty = state.users.filter { user in
            viewmodel.filter(with: "a", user: user)
        }
        
        XCTAssert(userEmpty.count == 4)
    }

}
