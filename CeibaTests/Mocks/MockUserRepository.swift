//
//  MockUserRepository.swift
//  CeibaUITests
//
//  Created by Ronald Ivan Ruiz Poveda on 9/12/20.
//


import Foundation
@testable import Ceiba


public class MockUserRepository  {
    var value: String = ""
    var type: TypeText = .empty
}

enum TypeText {
    case empty
    case full
}

extension MockUserRepository: IUserRepository {
    convenience init(type: TypeText = .empty) {
        self.init()
        self.type = type
    }
    
    public func getUserFromService(completion: @escaping ([User]) -> Void) {
        switch self.type {
        case .empty:
            completion([User]())
            break;
        default:
            completion(readDataFromJson())
            break;
        }
        
    }
    
    func readDataFromJson() -> [User] {
        var users = [User]()
        
        users.append(User(name: "carlos"))
        users.append(User(name: "Daniel"))
        users.append(User(name: "Ceiba"))
        users.append(User(name: "Zzz"))
        users.append(User(name: "ronal"))
        
        return users
    }
    
    
    public func saveOnDevice() {
        
    }
}
