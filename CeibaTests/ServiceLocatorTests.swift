//
//  ServiceLocatorTests.swift
//  CeibaTests
//
//  Created by Ronald Ivan Ruiz Poveda on 11/12/20.
//

import XCTest
@testable import Ceiba

class ServiceLocatorTest: XCTestCase  {
    
    func test_service_locator_register() {
        ServiceLocator.register(UserRepository() as IUserRepository)
        
        let depenedencies = ServiceLocator.shared.dependencies
        
        // Must not be empty
        XCTAssert(!depenedencies.isEmpty)
        
        // Must return the instance
        XCTAssert(depenedencies["IUserRepository.Protocol"] != nil)
        
        // Must return nil, because the services was registered with protocol as key
        XCTAssert(depenedencies["UserRepository.class"] == nil)
    }
    
    func test_service_locator_resolve() {
        ServiceLocator.register(UserRepository() as IUserRepository)
        
        let depenedencies = ServiceLocator.shared.dependencies
        
        let instance: IUserRepository? = ServiceLocator.resolve()
        
        // Must not be empty
        XCTAssert(instance != nil)
        
        // Must return the instance
        XCTAssert(depenedencies["IUserRepository.Protocol"] != nil)
    }
    
    func test_service_locator_unregister() {
        let service: IUserRepository = UserRepository() as IUserRepository
        ServiceLocator.register(service)
        
        ServiceLocator.unregister(service)
        
        let depenedencies = ServiceLocator.shared.dependencies
        
        // Must not be empty
        XCTAssert(depenedencies.isEmpty)
    }
}
